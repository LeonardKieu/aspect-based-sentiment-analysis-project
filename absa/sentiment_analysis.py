import tensorflow as tf
from keras.preprocessing.sequence import pad_sequences
from django.conf import settings
import pickle
import time

# # Set up for using Monkeylearn
# ml = MonkeyLearn('121d3d480928b512547ccf02ee3514f1521e7dea')
# data = [
#     "this iPhone is just only decent, not meet expectation!",
#     "Nghia carries team, we are free, so funny"
# ]
# model_id = 'cl_pi3C7JiL'

# Set up for using model
model = tf.keras.models.load_model(settings.KERAS_MODEL)
with open(settings.TOKENIZER_MODEL, 'rb') as handle:
    tokenizer = pickle.load(handle)


def analyze_sentiment(text, include_neutral=True):
    def decode_sentiment(score, include_neutral=True):
        if include_neutral:
            label = settings.NEUTRAL
            if score <= settings.SENTIMENT_THRESHOLDS[0]:
                label = settings.NEGATIVE
            elif score >= settings.SENTIMENT_THRESHOLDS[1]:
                label = settings.POSITIVE

            return label
        else:
            return settings.NEGATIVE if score < 0.5 else settings.POSITIVE

    start_at = time.time()
    # Tokenize text
    x_test = pad_sequences(tokenizer.texts_to_sequences([text]), maxlen=settings.SEQUENCE_LENGTH)
    # Predict
    score = model.predict([x_test])[0]
    # Decode sentiment
    decoded_label = decode_sentiment(score, include_neutral=include_neutral)

    return {
        "text": text,
        "sentiment_type": decoded_label,
        "sentiment_score": float(score),
        # "elapsed_time": time.time() - start_at
    }

# def analyze_sentiment_by_monkeylearn(opinions: list = data):
#     result = ml.classifiers.classify(model_id, opinions)
#     returned_results = []
#     for result in result.body:
#         needed_result = result['classifications'][0]
#         returned_result = {
#             "text": result['text'],
#             "tag_name": needed_result['tag_name'],
#             "confidence": needed_result['confidence']
#         }
#         returned_results.append(returned_result)
#     return returned_results


# def analyze_sentiment_mock(opinions: list = data):
#     returned_results = []
#     for opinion in opinions:
#         returned_result = {
#             "text": "opi x",
#             "tag_name": "positive",
#             "confidence": 0.8
#         }
#         returned_results.append(returned_result)
#     return returned_results

# # Google NLP
# client = language_v1.LanguageServiceClient.from_service_account_json(settings.CREDENTIAL_GOOGLE_PATH)
# def analyze_sentiment_by_Google(opinions: list = data):
#     def score2tagname(score):
#         if 0.25 < score <= 1:
#             return "positive"
#         elif -0.25 < score <= 0.25:
#             return "neutral"
#         else:
#             return "negative"
#
#     returned_results = []
#     for text in opinions:
#         document = language_v1.Document(content=text, type_=language_v1.Document.Type.PLAIN_TEXT)
#         sentiment = client.analyze_sentiment(request={'document': document}).document_sentiment
#         returned_result = {
#             "text": text,
#             "score": sentiment.score,
#             "tag_name": score2tagname(sentiment.score),
#             "magnitude": sentiment.magnitude
#         }
#         returned_results.append(returned_result)
#     return returned_results
